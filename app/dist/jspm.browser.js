SystemJS.config({
  baseURL: "/",
  paths: {
    "github:*": "assets/jspm_packages/github/*",
    "npm:*": "assets/jspm_packages/npm/*",
    "easrfd/": "js/"
  }
});
