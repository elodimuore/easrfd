import c3 from 'c3';

const load = (inputData) => {
  const
    dataSet = {
      x: [`x`],
      y: [`y`]
    };
  let total = 0;

  for (let i = 0; i < inputData.length; i++) {
    total += inputData[i];
    dataSet.x.push(total);
    dataSet.y.push(inputData[i]);
  }

  c3.generate({
    bindto: `#scatter`,
    data: {
      x: `x`,
      columns: [
        dataSet.x,
        dataSet.y
      ],
      type: `scatter`
    },
    tooltip: {
      contents: function (d, defaultTitleFormat, defaultValueFormat, color) {


          return '<table border="1" bordercolor="white" cellpadding="1" bgcolor="#e74c3c">'+
          '<tr>'+
            '<td><font color=white FACE="Geneva, Arial" size=3>X: '+ d[0].x +'</font></td>'+
            '<td><font color=white FACE="Geneva, Arial" size=3>Y: '+ d[0].value +'</font></td>'+
          "</tr>"+
          "</table>"
      }
    },
    axis: {
      x: {
        label: {
          text: `X Label (Scatter)`,
          position: `outer-left`
        },
        tick: {
          fit: false
        }
      },
      y: {
        label: {
          text: `Y Label (Scatter)`,
          position: `outer-top`
        },
        tick: {
          fit: false
        }
      }
    }
  });
  console.log(`Scatter Plot Loaded!`);
};

export default {
  load
};
