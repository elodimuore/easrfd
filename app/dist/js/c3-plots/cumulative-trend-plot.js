import c3 from 'c3';

const load = (inputData, dp = 1) => {
  const
    dataSet = {
      x: [`x`],
      y: [`y`]
    },
    total = inputData.reduce((p, c) => {
      return p + c;
    }, 0);

  for (let i = 0, j = 0; i < inputData.slice(0, inputData.length * dp).length; i++) {
    j += inputData[i];
    dataSet.x.push((j / total));
    dataSet.y.push(i);
  }

  c3.generate({
    bindto: `#cumulative`,
    data: {
      x: `x`,
      columns: [
        dataSet.x,
        dataSet.y
      ]
    },
    tooltip: {
      contents: function (d, defaultTitleFormat, defaultValueFormat, color) {


          return '<table border="1" bordercolor="white" cellpadding="1" bgcolor="#e74c3c">'+
          '<tr>'+
            '<td><font color=white FACE="Geneva, Arial" size=3>X: '+ d[0].x +'</font></td>'+
            '<td><font color=white FACE="Geneva, Arial" size=3>Y: '+ d[0].value +'</font></td>'+
          "</tr>"+
          "</table>"
      }
    },
    axis: {
      x: {
        label: {
          text: `X Label (Cumulative)`,
          position: `outer-left`
        },
        tick: {
          fit: false
        }
      },
      y: {
        label: {
          text: `Y Label (Cumulative)`,
          position: `outer-top`
        },
        tick: {
          fit: false
        }
      }
    }
  });
  console.log(`Cumulative Plot Loaded!`);
};

export default {
  load
};
