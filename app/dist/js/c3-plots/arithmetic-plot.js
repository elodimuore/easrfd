import c3 from 'c3';
import d3 from 'd3';

const load = (inputData) => {
  const dataSet = {
    x: [`x`],
    y: [`y`]
  };

  for (let i = 0; i < inputData.length; i++) {
    dataSet.x.push(i);
    if (inputData[i] === 0) {
      dataSet.y.push(0);
    } else {
      dataSet.y.push(i / inputData[i]);
    }
  }

  c3.generate({
    bindto: `#arithmetic`,
    data: {
      x: `x`,
      columns: [
        dataSet.x,
        dataSet.y
      ]
    },
    tooltip: {
      contents: function (d, defaultTitleFormat, defaultValueFormat, color) {


          return '<table border="1" bordercolor="white" cellpadding="1" bgcolor="#e74c3c">'+
          '<tr>'+
            '<td><font color=white FACE="Geneva, Arial" size=3>X: '+ d[0].x +'</font></td>'+
            '<td><font color=white FACE="Geneva, Arial" size=3>Y: '+ d[0].value +'</font></td>'+
          "</tr>"+
          "</table>"
      }
    },
    axis: {
      x: {
        label: {
          text: `X Label (Arithmetic)`,
          position: `outer-left`
        },
        tick: {
          fit: false,
          format: d3.format("s")
        }
      },
      y: {
        label: {
          text: `Y Label (Arithmetic)`,
          position: `outer-top`
        },
        tick: {
          fit: false
        }
      }
    }
  });

  console.log(`Arithmetic loaded!`);
};

export default {
  load
};
