import c3 from 'c3';

const load = (inputData) => {
  const dataSet = {
    x: [`x`],
    y: [`y`]
  };
  let total = 0;

  for (let i = 0; i < inputData.length; i++) {
    total += inputData[i];
    dataSet.x.push(total);
    dataSet.y.push(inputData[i]);
  }

  c3.generate({
    bindto: `#timeseries`,
    data: {
      x: `x`,
      columns: [
        dataSet.x,
        dataSet.y
      ]
    },
    tooltip: {
      contents: function (d, defaultTitleFormat, defaultValueFormat, color) {


          return '<table border="1" bordercolor="white" cellpadding="1" bgcolor="#e74c3c">'+
          '<tr>'+
            '<td><font color=white FACE="Geneva, Arial" size=3>X: '+ d[0].x +'</font></td>'+
            '<td><font color=white FACE="Geneva, Arial" size=3>Y: '+ d[0].value +'</font></td>'+
          "</tr>"+
          "</table>"
      }
    },
    axis: {
      x: {
        label: {
          text: `X Label (Time Series)`,
          position: `outer-left`
        },
        tick: {
          fit: false
        }
      },
      y: {
        label: `Y Label (Time Series)`,
        position: `outer-top`
      }
    }
  });

  console.log(`Time Series Loaded!`);
};

export default {
  load
};
