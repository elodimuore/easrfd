import d3 from 'd3';
import utils from '../utils/utils.js';

const load = (inputData) => {
  const
    dataSet = [],
    margin = {
      top: 20,
      right: 20,
      bottom: 30,
      left: 40
    },
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom,
    scatterSVG = d3.select(`#view`)
        .append(`svg`)
        .attr(`width`, width)
        .attr(`height`, height),
    xScale = (xMax, rMax) => {
      return d3.scale.linear().domain([0, xMax]).range([0, rMax]);
    },
    xAxis = (x, rMax) => {
      return d3.svg.axis().scale(xScale(x, rMax)).orient(`bottom`);
    },
    yScale = (yMax, rMin) => {
      return d3.scale.linear().domain([0, yMax]).range([rMin, 0]);
    },
    yAxis = (y, rMin) => {
      return d3.svg.axis().scale(yScale(y, rMin)).orient(`left`);
    };
  let total = 0;

  for (let i in inputData) {
    total += inputData[i];
    dataSet.push([total, inputData[i]]);
  }

  scatterSVG.append(`g`)
    .attr(`class`, `scatter-axis`)
    .attr(`transform`, `translate(0, ${height})`)
    .call(xAxis(utils.pairMax(dataSet), width));

  scatterSVG.append(`g`)
    .attr(`class`, `scatter-axis`)
    .attr(`transform`, `translate(0, 0)`)
    .call(yAxis(utils.max(inputData), height));

  scatterSVG.selectAll(`circle`)
      .data(dataSet)
      .enter()
      .append(`circle`)
      .attr(`class`, `scatter-circle`)
      .attr(`cx`, (data) => {
        return data[0];
      })
      .attr(`cy`, (data) => {
        return data[1];
      })
      .attr(`r`, 3);
};

export default {
  load
};
