import arithmeticPlot from './c3-plots/arithmetic-plot.js';
import cumulativePlot from './c3-plots/cumulative-trend-plot.js';
import histogramPlot from './c3-plots/histogram-plot.js';
import scatterPlot from './c3-plots/scatter-plot.js';
import timeseriesPlot from './c3-plots/time-series-plot.js';
import utils from './utils/utils.js';

if (window.File && window.FileReader && window.FileList && window.Blob) {
  document.querySelector(`#upload`).onchange = (e) => {
    const
      file = e.target.files[0],
      reader = new FileReader();

    reader.onload = () => {
      const inputData = utils.serialize(reader.result);

      scatterPlot.load(inputData);
      cumulativePlot.load(inputData);
      histogramPlot.load(inputData);
      arithmeticPlot.load(inputData);
      timeseriesPlot.load(inputData);
    };

    reader.readAsText(file);
  };
  console.log(`Main loaded!`);
} else {
  console.log(`The File APIs are not fully supported in this browser.`);
}
