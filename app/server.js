import bs from 'browser-sync';
import chokidarEmitter from 'chokidar-socket-emitter';
import compression from 'compression';
import express from 'express';
import fs from 'fs';
import morgan from 'morgan';
import favicon from 'serve-favicon';
import stylus from 'stylus';

import routes from './routes.js';

const
  app = express(),
  env = process.env.NODE_ENV || `production`,
  logStream = fs.createWriteStream(`${__dirname}/logs/access.log`, {flags: `a`}),
  port = (env !== `production`) ? 2368 : 3000,
  viewPath = `${__dirname}/app/views`;

app.set(`views`, viewPath);
app.set(`view engine`, `jade`);
app.set(`port`, port);

app.use(compression());
app.use(favicon(`${__dirname}/app/dist/assets/img/favicon.ico`));
app.use(morgan(`combined`, {
  skip: (req, res) => {
    return res.statusCode < 400;
  },
  stream: logStream
}));
app.use(stylus.middleware({
  src: `${__dirname}/app/styles`,
  dest: `${__dirname}/app/dist/css`,
  compress: true,
  compile: (str, path) => {
    return stylus(str)
        .set(`filename`, path)
        .set(`compress`, true);
  }
}));

app.get(`/`, routes.index);

app.use(express.static(`${__dirname}/app/dist`));

app.use(routes.error);
app.use(routes.notFound);

app.listen(port, () => {
  if (process.env.NODE_ENV !== `production`) {
    chokidarEmitter({port: 5776});
    bs({
      files: [
        `./app/*/*.{jade,styl}`,
        `./app/dist/js/c3-plots/*.js`,
        `./app/dist/js/easrfd.js`
      ],
      open: false,
      proxy: `localhost:${app.get(`port`)}`
    });
    console.log(`listening on port ${app.get(`port`)} w/ browser-sync`);
  }
});
